import React from 'react';

class App extends React.Component {
	render () {
		return (
			<p style={{fontSize: '1.5em', textAlign: 'center'}}>PWA React Boilerplate</p>
		)
	}
}

export default App;
