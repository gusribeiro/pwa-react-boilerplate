import React from 'react';
import ReactDOM from 'react-dom';
import App from './source/app';

ReactDOM.render(
	<App />,
	document.getElementById('app'),
);

if ('serviceWorker' in navigator) {
	navigator.serviceWorker.register('./serviceWorker.js')
}
